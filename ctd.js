var connectTheDots = function() {
  function solve(id) {
    var pjs = Processing.getInstanceById(id);
    pjs.solve();
  }

  // Creates edges connecting the nodes as ordered in a cycle
  function getCyclicEdges(nodes) {
    var edges = new Array(),
        nodeIdx,
        currentNode,
        previousNode;

    for (nodeIdx = 0; nodeIdx < nodes.length; nodeIdx++) {
      currentNode = nodes[nodeIdx];
      if (null != previousNode) {
        edges.push(new Edge(previousNode, currentNode));
      }
      previousNode = currentNode;
    }

    // The last node should connect to the first as another edge.
    edges.push(new Edge(nodes[nodes.length - 1], nodes[0]));

    return edges;
  }

  return {
    solve: solve,
    getCyclicEdges: getCyclicEdges
  }
}();
