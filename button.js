Button = function() {
  var self = function(x0, y0, w, h, text) {
    this.x0 = x0;
    this.y0 = y0;
    this.x1 = x0 + w;
    this.y1 = y0 + h;
    this.w = w;
    this.h = h;
    this.text = text;
  }

  return self;
}();
