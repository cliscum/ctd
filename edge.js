Edge = function() {
  var self = function(node0, node1) {
    this.node0 = node0;
    this.node1 = node1;
    this.theta = theta;
  }

  function theta() {
    return Math.atan2(this.node0.x - this.node1.x,
                      this.node0.y - this.node1.y) * 180 / Math.PI;
  }

  return self;
}();
