var canvasW = 800,
    canvasH = 600,
    nodeWeight = 10,
    edgeWeight = 1,
    nodes,
    button,
    isSolved;

function random(max) {
  return Math.floor(Math.random() * max);
}

function drawNode(node) {
  stroke(random(225), random(225), random(225));
  strokeWeight(nodeWeight);
  point(node.x, node.y);
}

function drawEdge(edge) {
  stroke(0);
  strokeWeight(edgeWeight);
  line(edge.node0.x, edge.node0.y, edge.node1.x, edge.node1.y);
}

function drawButton(label) {
  var w = 100,
      h = 40,
      labelSize = 28;

  button = new Button((canvasW / 2) - (w / 2),
                      canvasH - (2 * h),
                      w, h, label);
  stroke(0);
  strokeWeight(1);
  fill(245);
  rect(button.x0, button.y0, button.w, button.h);

  stroke(0);
  fill(0);
  textSize(labelSize);
  float tWidth = textWidth(button.text);
  text(button.text,
       button.x0 + ((button.w - tWidth) / 2),
       button.y0 + labelSize);
}

function makeDot(x, y) {
  var node = new Node(x, y);
  nodes.push(node);
  drawNode(node);
}

void solve() {
  var edges = new Array(),
      edgeIdx;

  edges = PolarCoordinateSolver.solve(nodes);

  for (edgeIdx = 0; edgeIdx < edges.length; edgeIdx++) {
    drawEdge(edges[edgeIdx]);
  }

  drawButton("Reset");
  isSolved = true;
}

function reset() {
  fill(250);
  rect(0, 0, canvasW - 1, canvasH - 1);
  nodes = new Array();
  drawButton("Solve");
  isSolved = false;
  generate(25);
}

function generate(count) {
  var i;
  for (i = 0; i < count; i++) {
    makeDot(random(canvasW), random(canvasH));
  }
}

void setup() {
  size(canvasW, canvasH);
  reset();
}

void mousePressed() {
  if (mouseX >= button.x0 && mouseX <= button.x1
      && mouseY >= button.y0 && mouseY <= button.y1)
  {
    if (isSolved) {
      reset();
    }
    else {
      solve();
    }
  }
  else {
    makeDot(mouseX, mouseY);
  }
}
