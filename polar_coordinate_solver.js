var PolarCoordinateSolver = function() {
  function bounds(nodes) {
    var i,
        topLeft = new Node(1<<30, 1<<30),
        bottomRight = new Node(0, 0);
    for (i = 0; i < nodes.length; i++) {
      if (nodes[i].x < topLeft.x) topLeft.x = nodes[i].x;
      if (nodes[i].y < topLeft.y) topLeft.y = nodes[i].y;
      if (nodes[i].x > bottomRight.x) bottomRight.x = nodes[i].x;
      if (nodes[i].y > bottomRight.y) bottomRight.y = nodes[i].y;
    }
    return [topLeft, bottomRight];
  }

  function sort(origin, nodes) {
    return nodes.sort(function (a, b) {
      return new Edge(origin, a).theta() - new Edge(origin, b).theta();
    });
  }

  function solve(nodes) {
    var edges,
        edgeIdx,
        bNodes = bounds(nodes),
        origin = new Node(parseInt((bNodes[0].x + bNodes[1].x) / 2),
                          parseInt((bNodes[0].y + bNodes[1].y) / 2)),
        sortedNodes = sort(origin, nodes);

    return connectTheDots.getCyclicEdges(sortedNodes);
  }

  return {
    solve: solve
  }
}();
